package com.lib;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.Random;

public class androidLib {

    public static double getCurrentVersion(Activity ac) {
        try {
            double currentVersion = Double.parseDouble(ac.getPackageManager()
                    .getPackageInfo(ac.getPackageName(), 0).versionName);

            return currentVersion;
        } catch (Exception e) {
        }
        return 0f;

    }

    public static void sendSMS(@NonNull Activity activity,@NonNull String number,@NonNull String body) {

        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + number));
        sendIntent.putExtra("sms_body", body);
        activity.startActivity(Intent.createChooser(sendIntent, "SHARE USING"));
    }

    public static void sendEmail(Activity activity, String[] recipients,
                                 String subject, String body) {
        // String[] recipients = new String[] { "", "", };
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + recipients[0]));
        intent.putExtra("subject", subject);
        intent.putExtra("body", body);
        activity.startActivity(intent);
    }

    public static void playVideo(Activity activity, String movieurl) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(movieurl));
        activity.startActivity(intent);
    }

    public static void openUrl(Activity activity, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
        activity.startActivity(intent);
    }

    public static void openMarketUrl(Activity activity, String packageName) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
            activity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void launchMarket(Activity activity) {
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(activity, "Couldnt_launch_market", Toast.LENGTH_LONG)
                    .show();
        }
    }

    public static boolean hasSDCard() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)
                || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
            return true;
        return false;
    }

    public static boolean checkSDAvailable(int required) {
        File sdCard = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(sdCard.getPath());
        long bytesAvailable = (long) stat.getBlockSize()
                * (long) stat.getAvailableBlocks();
        // int mbAvailable = (int) (bytesAvailable / 1048576);
        int mbAvailable = (int) (bytesAvailable);
        if (mbAvailable >= required) {
            return true;
        }
        return false;
    }

    public static void reStartAcivity(Activity ac) {
        if (ac != null) {
            Intent intent = ac.getIntent();
            ac.overridePendingTransition(0, 0);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            ac.finish();

            ac.overridePendingTransition(0, 0);
            ac.startActivity(intent);
        }
    }

    public static void showMemory(Activity ac) {
        MemoryInfo mi = new MemoryInfo();
        ActivityManager activityManager = (ActivityManager) ac
                .getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        double availableMegs = mi.availMem / 1024;
        Log.w("Free memory ", "" + availableMegs);
    }

    public static void callPhone(Activity ac, String number) {
        String uri = "tel:" + number;
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(uri));
        ac.startActivity(intent);
    }

    public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        //int randomNum = rand.nextInt((max - min) + 1) + min;
        int randomNum = rand.nextInt((max - min)) + min;
        return randomNum;
    }

    public static void freeMemorry() {
        try {
            System.gc();

        } catch (Exception e) {
        }

    }

    public static int parseInt(String number) {
        try {
            int rs = Integer.parseInt(number);
            return rs;
        } catch (Exception e) {
        }
        return 0;
    }
}
