package com.lib.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.philong.smsmarketing.utils.Constants;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

public class StorageUtils {
    private static Context context;

    public static void setContext(Context context) {
        StorageUtils.context = context;
    }

    public static byte[] readFileAsByte(String filePath)
            throws java.io.IOException {
        byte[] buffer = new byte[(int) new File(filePath).length()];
        BufferedInputStream f = new BufferedInputStream(new FileInputStream(
                filePath));
        f.read(buffer);
        return buffer;
    }

    public static void save(String key, String data) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(key, data);
        editor.apply();
    }

    public static String load(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(key, null);
    }

    public static void savePath(String filePath) {
        save(Constants.FILE_PATH, filePath);
    }

    public static String getPath() {
        return load(Constants.FILE_PATH);
    }
}
