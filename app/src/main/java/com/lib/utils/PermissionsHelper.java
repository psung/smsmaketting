package com.lib.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;

//https://www.captechconsulting.com/blogs/runtime-permissions-best-practices-and-how-to-gracefully-handle-permission-removal
public class PermissionsHelper {
    public static final int REQUEST_RECORD = 10001;
    public static final int REQUEST_CAMERA = 10002;
    public static final int REQUEST_STORATE = 10003;

    public static PermissionsHelper instance;
    public static PermissionsHelper getInstance() {
        if (instance == null) {
            instance = new PermissionsHelper();
        }
        return instance;
    }

    private String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private String[] PERMISSIONS_CALENDAR = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR
    };
    public static String[] PERMISSIONS_CAMERA = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA
    };
    private String[] PERMISSIONS_CONTACTS = {
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS
    };
    private String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private String[] PERMISSIONS_MICROPHONE = {
            Manifest.permission.RECORD_AUDIO
    };
    private String[] PERMISSIONS_PHONE = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.WRITE_CALL_LOG,
            Manifest.permission.ADD_VOICEMAIL,
            Manifest.permission.USE_SIP,
            Manifest.permission.PROCESS_OUTGOING_CALLS
    };
    private String[] PERMISSIONS_SENSORS = {
            Manifest.permission.BODY_SENSORS
    };
    private String[] PERMISSIONS_SMS = {
            Manifest.permission.SEND_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_WAP_PUSH,
            Manifest.permission.RECEIVE_MMS
    };

    private String[] PERMISSIONS_RECORD = {
            Manifest.permission.RECORD_AUDIO};

    public boolean verifyStoragePermissions(Activity activity) {
        String[] listPermissionsNeedRequest = getListPermissionsNeedRequest(activity, PERMISSIONS_STORAGE);
        if (listPermissionsNeedRequest.length == 0) {
            return true;
        }

        ActivityCompat.requestPermissions(activity, listPermissionsNeedRequest, REQUEST_STORATE);
        return false;
    }

    public boolean verifyCameraPermissions(Activity activity) {

        String[] listPermissionsNeedRequest = getListPermissionsNeedRequest(activity, PERMISSIONS_CAMERA);
        if (listPermissionsNeedRequest.length == 0) {
            return true;
        }

        ActivityCompat.requestPermissions(activity, listPermissionsNeedRequest, REQUEST_CAMERA);
        return false;
    }

    public boolean verifyRecordAudioPermissions(Activity activity) {

        String[] listPermissionsNeedRequest = getListPermissionsNeedRequest(activity, PERMISSIONS_RECORD);
        if (listPermissionsNeedRequest.length == 0) {
            return true;
        }

        ActivityCompat.requestPermissions(activity, listPermissionsNeedRequest, REQUEST_RECORD);
        return false;
    }

    public boolean checkRecordAudioPermissions(Activity activity) {
        String[] listPermissionsNeedRequest = getListPermissionsNeedRequest(activity, PERMISSIONS_RECORD);
        return listPermissionsNeedRequest.length == 0;
    }

    private String[] getListPermissionsNeedRequest(Activity activity, String[] list) {
        ArrayList<String> listPermission = new ArrayList<>();
        for ( String permission : list) {
            int value = ActivityCompat.checkSelfPermission(activity,
                    permission);
            if (value != PackageManager.PERMISSION_GRANTED) {
                listPermission.add(permission);
            }
        }

        String[] result = new String[listPermission.size()];
        result = listPermission.toArray(result);
        return result;
    }

    public static boolean isPermissionGranted(String[] grantPermissions, int[] grantResults,
                                              String permission) {
        for (int i = 0; i < grantPermissions.length; i++) {
            if (permission.equals(grantPermissions[i])) {
                return grantResults[i] == PackageManager.PERMISSION_GRANTED;
            }
        }
        return false;
    }
}

