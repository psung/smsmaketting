package com.philong.smsmarketing.modelview;

import com.philong.smsmarketing.model.DataModel;
import com.philong.smsmarketing.model.UserModel;

import java.io.Serializable;
import java.util.ArrayList;

public class DataModelView implements Serializable {
    private String content;
    private int delayTimeSecond;
    private ArrayList<UserModelView> listUser;

    public DataModelView(DataModel dataModel) {
        content = dataModel.getContent();
        delayTimeSecond = dataModel.getDelayTimeSecond();
        listUser = new ArrayList<>();
        ArrayList<UserModel> listUserModel = dataModel.getListUser();
        if (listUser != null) {

            for (UserModel userModel : listUserModel) {
                UserModelView userModelView = new UserModelView(userModel);
                listUser.add(userModelView);
            }
        }
    }

    public String getContent() {
        return content;
    }

    public int getDelayTimeSecond() {
        return delayTimeSecond;
    }

    public ArrayList<UserModelView> getListUser() {
        return listUser;
    }

    public ArrayList<UserModelView> getListUserChecked() {
        ArrayList<UserModelView> listUserChecked = new ArrayList<>();
        if (listUser != null) {
            for (UserModelView userModelView : listUser) {
                if (userModelView.isChecked()) {
                    listUserChecked.add(userModelView);
                }
            }
        }
        return listUserChecked;
    }

    public int getUserPositionByPhoneAndName(String phone, String name) {
        if (listUser != null && phone != null && name != null) {
            int size = listUser.size();
            for (int i = 0; i < size; i++) {
                UserModelView userModelView = listUser.get(i);
                if (phone.equalsIgnoreCase(userModelView.getPhone())
                        && (name.equalsIgnoreCase(userModelView.getName()))
                ) {
                    return i;
                }
            }
        }
        return -1;
    }
}
