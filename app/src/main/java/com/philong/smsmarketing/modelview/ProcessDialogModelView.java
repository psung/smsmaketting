package com.philong.smsmarketing.modelview;

import java.io.Serializable;

public class ProcessDialogModelView implements Serializable {
    private String message;
    private String username;
    private int process;
    private int processMax;
    private String phone;

    public ProcessDialogModelView(String username, String phone, String message, int processMax, int process) {
        this.message = message;
        this.username = username;
        this.processMax = processMax;
        this.process = process;
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public String getPhone() {
        return phone;
    }

    public int getProcessMax() {
        return processMax;
    }

    public String getUsername() {
        return username;
    }

    public int getProcess() {
        return process;
    }

}
