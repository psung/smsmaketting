package com.philong.smsmarketing.modelview;

import com.philong.smsmarketing.model.UserModel;

import java.io.Serializable;

public class UserModelView implements Serializable {
    private UserModel userModel;
    private boolean isChecked;

    UserModelView(UserModel userModel) {
        this.userModel = userModel;
        this.isChecked = false;
    }

    public String getName() {
        return userModel.getName();
    }

    public String getPhone() {
        return userModel.getPhone();
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
