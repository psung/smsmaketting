package com.philong.smsmarketing;

import android.app.Application;

import com.lib.utils.SimHelper;
import com.lib.utils.StorageUtils;


public class SMSApplication extends Application  {

    @Override
    public void onCreate() {
        super.onCreate();
        StorageUtils.setContext(getApplicationContext());
        SimHelper.setContext(getApplicationContext());
    }


}
