package com.philong.smsmarketing.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DataModel implements Serializable {
    private String content;
    private int delayTimeSecond;
    private ArrayList<UserModel> listUser;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<UserModel> getListUser() {
        return listUser;
    }

    public void setListUser(ArrayList<UserModel> listUser) {
        this.listUser = listUser;
    }

    public int getDelayTimeSecond() {
        return delayTimeSecond;
    }

    public void setDelayTimeSecond(int delayTimeSecond) {
        this.delayTimeSecond = delayTimeSecond;
    }
}
