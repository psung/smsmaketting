package com.philong.smsmarketing.view.activity;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.ProgressBar;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgress = null;
    private ProgressBar mProgressBar = null;

    public void showLoadingScreen() {
        if (mProgress == null) {
            mProgress = ProgressDialog.show(this, null, null);
            mProgressBar = new ProgressBar(this);
            mProgress.setContentView(mProgressBar);
            mProgress.setCancelable(false);
            Window window = mProgress.getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        }
        mProgress.setCanceledOnTouchOutside(false);
        if (!mProgress.isShowing()) {
            mProgress.show();
        }
    }

    public void hideLoadingScreen() {
        if (mProgress != null && mProgress.isShowing() && mProgressBar != null) {
            mProgress.dismiss();
            mProgress = null;
            mProgressBar = null;
        }
    }

}
