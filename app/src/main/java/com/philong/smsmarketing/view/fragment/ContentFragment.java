package com.philong.smsmarketing.view.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.philong.smsmarketing.R;
import com.philong.smsmarketing.databinding.FragmentContentBinding;
import com.philong.smsmarketing.viewmode.MainlViewModel;

public class ContentFragment extends BaseFragment {
    FragmentContentBinding mBinding;
    MainlViewModel mainlViewModel;

    public ContentFragment() {
        // Required empty public constructor
    }

    public static ContentFragment newInstance() {
        return new ContentFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_content, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mainlViewModel = ViewModelProviders.of(requireActivity()).get(MainlViewModel.class);
        mainlViewModel.getContentLiveData().observe(this, mainObser);
    }

    final Observer<String> mainObser = new Observer<String>() {
        @Override
        public void onChanged(@Nullable String content) {
            mBinding.txtContent.setText(content);
        }
    };
}
