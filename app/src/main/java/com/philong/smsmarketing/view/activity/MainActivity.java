package com.philong.smsmarketing.view.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.lib.utils.PermissionsHelper;
import com.philong.smsmarketing.R;
import com.philong.smsmarketing.databinding.ActivityMainBinding;
import com.philong.smsmarketing.modelview.ProcessDialogModelView;
import com.philong.smsmarketing.view.adapter.ViewPagerAdapter;
import com.philong.smsmarketing.view.fragment.ContentFragment;
import com.philong.smsmarketing.view.fragment.ReceiversFragment;
import com.philong.smsmarketing.viewmode.MainlViewModel;
import com.philong.smsmarketing.utils.Constants;
import com.lib.utils.UrlHelper;

public class MainActivity extends BaseActivity {
    private ActivityMainBinding mBinding;
    private ViewPagerAdapter adapter;

    private MainlViewModel mainlViewModel;

    private ProgressDialog progressDialogSending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        mBinding.selectFile.setOnClickListener(onClickListener);
        mBinding.btnSent.setOnClickListener(onClickListener);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewpager);
        setupViewPager(mBinding.viewpager);

        mainlViewModel = ViewModelProviders.of(this).get(MainlViewModel.class);
        mainlViewModel.getIsLoadErrorLiveData().observe(this, errorObser);
        mainlViewModel.getShowDialogSendLiveData().observe(this, processDialogModelViewObser);
        mainlViewModel.getTitleLiveData().observe(this, titleObser);
        mainlViewModel.getIsShowLoadingDialogLiveData().observe(this, loadingObser);
        mainlViewModel.getIsShowSelectFileLiveData().observe(this, selectFileObser);
        mainlViewModel.getIsShowViewPagerLiveData().observe(this, viewPagerObser);

        PermissionsHelper.getInstance().verifyStoragePermissions(this);

        mainlViewModel.init("aaa");
    }

    final Observer<Boolean> errorObser = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean error) {
            if (error != null && error) {
                Toast.makeText(MainActivity.this, getString(R.string.Read_file_error), Toast.LENGTH_LONG).show();
            }
        }
    };

    final Observer<ProcessDialogModelView> processDialogModelViewObser = new Observer<ProcessDialogModelView>() {
        @Override
        public void onChanged(@Nullable ProcessDialogModelView dataModel) {
            assert dataModel != null;
            if (dataModel.getProcess() == 0) {
                showDialogSendingProcess(dataModel.getMessage(), dataModel.getUsername(), dataModel.getPhone(), dataModel.getProcessMax());
            }
            if (dataModel.getProcess() == 100) {
                hideDialogSendingProcess();
            } else {
                updateProcessDialogprcess(dataModel.getMessage(), dataModel.getUsername(), dataModel.getPhone(), dataModel.getProcess(), dataModel.getProcessMax());
            }
        }
    };

    final Observer<String> titleObser = new Observer<String>() {
        @Override
        public void onChanged(@Nullable String text) {
            setTitle(text);
        }
    };
    final Observer<Boolean> loadingObser = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isShowLoading) {
            assert isShowLoading != null;
            if (isShowLoading) {
                showLoadingScreen();
            } else {
                hideLoadingScreen();
            }
        }
    };

    final Observer<Boolean> selectFileObser = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isShowLoading) {
            assert isShowLoading != null;
            mBinding.selectFile.setVisibility(isShowLoading ? View.VISIBLE : View.GONE);
        }
    };

    final Observer<Boolean> viewPagerObser = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isShowLoading) {
            assert isShowLoading != null;
            mBinding.viewpager.setVisibility(isShowLoading ? View.VISIBLE : View.GONE);
        }
    };


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.selectFile:
                    if (PermissionsHelper.getInstance().verifyStoragePermissions(MainActivity.this)) {
                        selectFile();
                    }
                    break;
                case R.id.btnSent:
                    sentMessage();
                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.itemSelectFile:
                if (PermissionsHelper.getInstance().verifyStoragePermissions(this)) {
                    selectFile();
                }
                break;
            case R.id.itemAllItem:
                selectAllItems();
                break;
            case R.id.itemClearAll:
                clearAllItems();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void selectFile() {
        Intent openIntent = new Intent(Intent.ACTION_GET_CONTENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            openIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            openIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        openIntent.addCategory(Intent.CATEGORY_OPENABLE);
        openIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        openIntent.setType("*/*");
        openIntent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"*/*"});
        this.startActivityForResult(openIntent, Constants.REQUEST_PICK_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_PICK_FILE) {
            onActivityResultPickFile(resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case PermissionsHelper.REQUEST_STORATE:
                if (PermissionsHelper.isPermissionGranted(permissions, grantResults, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    selectFile();
                } else {
                    Toast.makeText(this, R.string.permission_toast_deny_read_storage, Toast.LENGTH_LONG).show();
                }
                break;


            default:
                break;
        }
    }

    private void onActivityResultPickFile(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri contentUri = data.getData();
            String path = UrlHelper.getRealPathFromURI(this, contentUri);
            if (!TextUtils.isEmpty(path)) {
                mainlViewModel.loadData(path);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        if (adapter != null) {
            adapter.clearChildren();
        } else {
            adapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(adapter);
        }

        adapter.addFragment(ContentFragment.newInstance(),
                getString(R.string.Content));
        adapter.addFragment(ReceiversFragment.newInstance(),
                getString(R.string.Receivers));

        adapter.notifyDataSetChanged();
    }

    private void showDialogSendingProcess(String content, String username, String phone, int processMax) {
        progressDialogSending = new ProgressDialog(this);
        progressDialogSending.setTitle("Sending to: " + username + " : " + phone);
        progressDialogSending.setMessage(content);
        progressDialogSending.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialogSending.setMax(processMax);
        progressDialogSending.setCancelable(true);
        progressDialogSending.setIndeterminate(false);
        progressDialogSending.show();

        progressDialogSending.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mainlViewModel.onDismissDialogProcessSendMessage();
                progressDialogSending = null;
            }
        });
    }

    private void hideDialogSendingProcess() {
        if (progressDialogSending != null) {
            progressDialogSending.dismiss();
        }
    }

    private void updateProcessDialogprcess(String content, String username, String phone, int process, int processMax) {
        if (progressDialogSending != null) {
            progressDialogSending.setProgress(process);
            progressDialogSending.setTitle("Sending to: " + username + " : " + phone);
            progressDialogSending.setMessage(content);
        } else {
            showDialogSendingProcess(content, username, phone, processMax);
        }
    }

    private void setTitle(String title) {
        mBinding.toolbarTitle.setVisibility(View.VISIBLE);
        mBinding.toolbarTitle.setText(title);
    }

    private void selectAllItems() {
        mainlViewModel.selectAllItems();
    }

    private void clearAllItems() {
        mainlViewModel.clearAllItems();
    }

    private void sentMessage() {
        mainlViewModel.sentMessage();
    }

}
