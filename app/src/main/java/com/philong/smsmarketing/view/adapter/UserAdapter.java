package com.philong.smsmarketing.view.adapter;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.philong.smsmarketing.R;
import com.philong.smsmarketing.databinding.RvItemUserBinding;
import com.philong.smsmarketing.listener.UserListener;
import com.philong.smsmarketing.modelview.UserModelView;

import java.util.ArrayList;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<UserModelView> listUsers;
    private UserListener userListener;

    class UserViewHolder extends RecyclerView.ViewHolder {
        RvItemUserBinding rvItemUserBinding;

        private UserViewHolder(@NonNull RvItemUserBinding binding) {
            super(binding.getRoot());
            rvItemUserBinding = binding;
        }
    }

    public UserAdapter(Context context, UserListener listener) {
        inflater = LayoutInflater.from(context);
        userListener = listener;
    }

    public void setListUsers(ArrayList<UserModelView> listUsers) {
        this.listUsers = listUsers;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvItemUserBinding binding = DataBindingUtil.inflate(inflater, R.layout.rv_item_user, parent, false);
        return new UserViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserViewHolder holder, int position) {
        if (listUsers != null) {
            final UserModelView userModelView = listUsers.get(position);
            holder.rvItemUserBinding.txtUser.setText(userModelView.getName());
            holder.rvItemUserBinding.txtPhone.setText(userModelView.getPhone());

            holder.rvItemUserBinding.checkboxSelect.setTag(position);
            holder.rvItemUserBinding.checkboxSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int pos = (int) buttonView.getTag();
                    userListener.onCheckedChanged(pos, isChecked);
//                    userModelView.setChecked(isChecked);
                }
            });
            holder.rvItemUserBinding.checkboxSelect.setChecked(userModelView.isChecked());
        } else {
            holder.rvItemUserBinding.txtUser.setText("");
            holder.rvItemUserBinding.txtPhone.setText("");
        }
    }

    @Override
    public int getItemCount() {

        if (listUsers != null) {
            return listUsers.size();
        }
        return 0;
    }
}