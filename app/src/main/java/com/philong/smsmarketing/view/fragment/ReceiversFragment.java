package com.philong.smsmarketing.view.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.philong.smsmarketing.R;
import com.philong.smsmarketing.databinding.FragmentReceiversBinding;
import com.philong.smsmarketing.listener.UserListener;
import com.philong.smsmarketing.modelview.UserModelView;
import com.philong.smsmarketing.view.adapter.UserAdapter;
import com.philong.smsmarketing.viewmode.MainlViewModel;

import java.util.ArrayList;
import java.util.Objects;

public class ReceiversFragment extends BaseFragment {
    FragmentReceiversBinding mBinding;
    UserAdapter userAdapter;
    ArrayList<UserModelView> listUser;
    LinearLayoutManager linearLayoutManager;
    MainlViewModel mainlViewModel;

    public ReceiversFragment() {
        // Required empty public constructor
    }

    public static ReceiversFragment newInstance() {
        return new ReceiversFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_receivers, container, false);
        mainlViewModel = ViewModelProviders.of(requireActivity()).get(MainlViewModel.class);
        mainlViewModel.getListUserLiveData().observe(this, listUserObser);
        mainlViewModel.getPositionUserInListRefreshLiveData().observe(this, positionUserInListRefreshObser);

        setupListUser();
        return mBinding.getRoot();
    }

    final Observer<Integer> positionUserInListRefreshObser = new Observer<Integer>() {
        @Override
        public void onChanged(@Nullable Integer postion) {
            if (postion != null && userAdapter != null) {
                userAdapter.notifyItemChanged(postion);
            }
        }
    };

    final Observer<ArrayList<UserModelView>> listUserObser = new Observer<ArrayList<UserModelView>>() {
        @Override
        public void onChanged(@Nullable ArrayList<UserModelView> users) {
            if (users != null && userAdapter != null) {
                listUser.clear();
                listUser.addAll(users);
                userAdapter.notifyDataSetChanged();
            }
        }
    };

    public void setupListUser() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.listUserRecyclerView.setLayoutManager(linearLayoutManager);

        mBinding.listUserRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getContext()),
                DividerItemDecoration.VERTICAL));

        userAdapter = new UserAdapter(mActivity, userListener);
        mBinding.listUserRecyclerView.setAdapter(userAdapter);
        listUser = new ArrayList<>();
        userAdapter.setListUsers(listUser);
    }

    UserListener userListener = new UserListener() {
        @Override
        public void onCheckedChanged(int postion, boolean isChecked) {
            mainlViewModel.onUserChekedChanged(postion, isChecked);
        }
    };
}
