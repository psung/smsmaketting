package com.philong.smsmarketing.utils;

import com.odsReader.reader.WorkSheets;
import com.philong.smsmarketing.model.DataModel;
import com.philong.smsmarketing.model.UserModel;
import com.lib.utils.StorageUtils;

import java.util.ArrayList;

public class DataManager {
    private static DataManager instance;

    public static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    public DataModel loadDataODS(String path) {
        try {
            if (path.endsWith(".ods")) {
                DataModel dataModel = new DataModel();
                byte[] dataFile = StorageUtils.readFileAsByte(path);

                WorkSheets s_wsLeftSide = WorkSheets.getWorkSheetsByString(dataFile);

                if (s_wsLeftSide == null) {
                    return null;
                }
                int nCurrentSheet = 0;
//                String strSheetname = s_wsLeftSide.getSheetName(nCurrentSheet);
                String time = s_wsLeftSide.getSheetCell(nCurrentSheet, 1, 0);
                dataModel.setDelayTimeSecond(Integer.parseInt(time));

                String content = s_wsLeftSide.getSheetCell(nCurrentSheet, 1, 1);
                dataModel.setContent(content);

                nCurrentSheet = 1;
//                strSheetname = s_wsLeftSide.getSheetName(nCurrentSheet);
                int nRow = s_wsLeftSide.getSheetRow(nCurrentSheet);

                ArrayList<UserModel> listUsers = new ArrayList<>();
                for (int i = 1; i < nRow; i++) {
                    String name = s_wsLeftSide.getSheetCell(nCurrentSheet, 0, i);
                    String phone = s_wsLeftSide.getSheetCell(nCurrentSheet, 1, i);

                    UserModel userModel = new UserModel(name, phone);
                    listUsers.add(userModel);
                }
                dataModel.setListUser(listUsers);

                return dataModel;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
