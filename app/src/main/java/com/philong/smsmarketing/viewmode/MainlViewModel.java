package com.philong.smsmarketing.viewmode;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.text.TextUtils;

import com.lib.utils.StorageUtils;
import com.philong.smsmarketing.model.DataModel;
import com.philong.smsmarketing.modelview.DataModelView;
import com.philong.smsmarketing.modelview.ProcessDialogModelView;
import com.philong.smsmarketing.modelview.UserModelView;
import com.philong.smsmarketing.utils.DataManager;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainlViewModel extends ViewModel {

    private MutableLiveData<DataModelView> dataModelViewLiveData;
    private MutableLiveData<String> contentLiveData;
    private MutableLiveData<ArrayList<UserModelView>> listUserLiveData;
    private MutableLiveData<Integer> positionUserInListRefreshLiveData;
    private MutableLiveData<String> titleLiveData;
    private MutableLiveData<Boolean> isShowLoadingDialogLiveData;
    private MutableLiveData<Boolean> isLoadErrorLiveData;
    private MutableLiveData<Boolean> isShowViewPagerLiveData;
    private MutableLiveData<Boolean> isShowSelectFileLiveData;
    private MutableLiveData<ProcessDialogModelView> showDialogSendLiveData;

    private ExecutorService service = Executors.newSingleThreadExecutor();
    private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    public MutableLiveData<Integer> getPositionUserInListRefreshLiveData() {
        if (positionUserInListRefreshLiveData == null) {
            positionUserInListRefreshLiveData = new MutableLiveData<>();
        }
        return positionUserInListRefreshLiveData;
    }

    public MutableLiveData<ArrayList<UserModelView>> getListUserLiveData() {
        if (listUserLiveData == null) {
            listUserLiveData = new MutableLiveData<>();
        }
        return listUserLiveData;
    }

    public MutableLiveData<String> getContentLiveData() {
        if (contentLiveData == null) {
            contentLiveData = new MutableLiveData<>();
        }
        return contentLiveData;
    }

    public MutableLiveData<Boolean> getIsLoadErrorLiveData() {
        if (isLoadErrorLiveData == null) {
            isLoadErrorLiveData = new MutableLiveData<>();
        }
        return isLoadErrorLiveData;
    }

    public MutableLiveData<ProcessDialogModelView> getShowDialogSendLiveData() {
        if (showDialogSendLiveData == null) {
            showDialogSendLiveData = new MutableLiveData<>();
        }
        return showDialogSendLiveData;
    }

    private MutableLiveData<DataModelView> getDataModelViewLiveData() {
        if (dataModelViewLiveData == null) {
            dataModelViewLiveData = new MutableLiveData<>();
        }
        return dataModelViewLiveData;
    }

    public MutableLiveData<String> getTitleLiveData() {
        if (titleLiveData == null) {
            titleLiveData = new MutableLiveData<>();
        }
        return titleLiveData;
    }

    public MutableLiveData<Boolean> getIsShowLoadingDialogLiveData() {
        if (isShowLoadingDialogLiveData == null) {
            isShowLoadingDialogLiveData = new MutableLiveData<>();
        }
        return isShowLoadingDialogLiveData;
    }

    public MutableLiveData<Boolean> getIsShowViewPagerLiveData() {
        if (isShowViewPagerLiveData == null) {
            isShowViewPagerLiveData = new MutableLiveData<>();
        }
        return isShowViewPagerLiveData;
    }

    public MutableLiveData<Boolean> getIsShowSelectFileLiveData() {
        if (isShowSelectFileLiveData == null) {
            isShowSelectFileLiveData = new MutableLiveData<>();
        }
        return isShowSelectFileLiveData;
    }

    public void init(String title) {
        final String path = StorageUtils.getPath();

        getTitleLiveData().postValue(title);
        if (!TextUtils.isEmpty(path)) {
            getIsShowLoadingDialogLiveData().postValue(true);

            loadData(path);
        }
    }

    public void loadData(final String path) {
        if (!TextUtils.isEmpty(path)) {
            getIsShowLoadingDialogLiveData().postValue(true);

            service.submit(new Runnable() {
                @Override
                public void run() {
                    DataModel dataModel = DataManager.getInstance().loadDataODS(path);
                    if (dataModel != null) {
                        StorageUtils.savePath(path);

                        DataModelView dataModelView = new DataModelView(dataModel);
                        getDataModelViewLiveData().postValue(dataModelView);
                        getContentLiveData().postValue(dataModelView.getContent());
                        getListUserLiveData().postValue(dataModelView.getListUser());

                        getIsShowViewPagerLiveData().postValue(true);
                        getIsShowSelectFileLiveData().postValue(false);
                        getIsShowLoadingDialogLiveData().postValue(false);
                    } else {
                        getIsLoadErrorLiveData().postValue(true);
                        getIsShowViewPagerLiveData().postValue(false);
                        getIsShowSelectFileLiveData().postValue(true);
                        getIsShowLoadingDialogLiveData().postValue(false);
                    }
                }
            });
        } else {
            getIsShowViewPagerLiveData().postValue(false);
            getIsShowSelectFileLiveData().postValue(true);
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();

    }

    public void selectAllItems() {
        DataModelView data = getDataModelViewLiveData().getValue();
        if (data != null) {
            ArrayList<UserModelView> listUserlView = data.getListUser();
            if (listUserlView != null) {
                int size = listUserlView.size();
                for (int i = 0; i < size; i++) {
                    UserModelView userModelView = listUserlView.get(i);
                    userModelView.setChecked(true);
                    getPositionUserInListRefreshLiveData().setValue(i);
                }
            }
        }
    }

    public void clearAllItems() {
        DataModelView data = getDataModelViewLiveData().getValue();
        if (data != null) {
            ArrayList<UserModelView> listUserlView = data.getListUser();
            if (listUserlView != null) {
                int size = listUserlView.size();
                for (int i = 0; i < size; i++) {
                    UserModelView userModelView = listUserlView.get(i);
                    userModelView.setChecked(false);
                    getPositionUserInListRefreshLiveData().setValue(i);
                }
            }
        }
    }

    public void sentMessage() {
        DataModelView data = getDataModelViewLiveData().getValue();
        assert data != null;
        ArrayList<UserModelView> listUserChecked = data.getListUserChecked();
        assert listUserChecked != null;
        int size = listUserChecked.size();
        for (int i = 0; i < size; i++) {
            UserModelView userModelView = listUserChecked.get(i);
            int process = (int) (((float) (i + 1) / size) * 100);
            SendMessageProcess sendMessageProcess = new SendMessageProcess(userModelView.getName(), userModelView.getPhone(), data.getContent(), size, process);

            if (scheduler.isShutdown()) {
                scheduler = Executors.newSingleThreadScheduledExecutor();
            }
//            scheduler.schedule(sendMessageProcess, i, TimeUnit.SECONDS);
            scheduler.schedule(sendMessageProcess, i * data.getDelayTimeSecond(), TimeUnit.SECONDS);
//            System.out.println("%%% start " + userModelView.getName() + " process " + process + " " + data.getDelayTimeSecond());

        }
    }

    public void onDismissDialogProcessSendMessage() {
        scheduler.shutdownNow();
    }

    public void onUserChekedChanged(int postion, boolean isChecked) {
        DataModelView data = getDataModelViewLiveData().getValue();
        if (data != null) {
            ArrayList<UserModelView> listUserlView = data.getListUser();
            if (listUserlView != null) {

                UserModelView userModelView = listUserlView.get(postion);
                userModelView.setChecked(isChecked);
                getPositionUserInListRefreshLiveData().postValue(postion);
            }
        }
    }

    class SendMessageProcess implements Runnable {
        ProcessDialogModelView modelView;

        SendMessageProcess(String username, String phone, String message, int processMax, int process) {
            modelView = new ProcessDialogModelView(username, phone, message, processMax, process);
        }

        @Override
        public void run() {
            getShowDialogSendLiveData().postValue(modelView);

//            System.out.println("%%% end " + modelView.getUsername() + " process " + modelView.getProcess());
            DataModelView data = getDataModelViewLiveData().getValue();
            if (data != null) {
                int pos = data.getUserPositionByPhoneAndName(modelView.getPhone(), modelView.getUsername());
//                System.out.println("%%% pos " + pos);
                data.getListUser().get(pos).setChecked(false);
                getPositionUserInListRefreshLiveData().postValue(pos);

 //               SimHelper.sendSMS(modelView.getPhone(), modelView.getMessage());
            }
        }
    }
}
