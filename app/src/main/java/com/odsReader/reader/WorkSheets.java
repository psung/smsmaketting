package com.odsReader.reader;

import android.util.Log;

import java.io.FileInputStream;

public class WorkSheets {
	private WorkSheet[] s_awSheets;
	private int s_nSheet;
	private int s_nSheetType = SHEET_TYPE_INVALID;

	public WorkSheets() {
	}

	public WorkSheets(WorkSheet[] sheets) {
		s_awSheets = sheets;
		s_nSheet = sheets.length;
	}

	public WorkSheet[] getSheets() {
		return s_awSheets;
	}

	public int getSheetNumber() {
		return s_nSheet;
	}

	public String getSheetName(int sheet) {
		if (0 <= sheet && sheet < s_nSheet) {
			return s_awSheets[sheet].getName();
		}
		return null;
	}

	public int getSheetColumn(int sheet) {
		if (0 <= sheet && sheet < s_nSheet) {
			return s_awSheets[sheet].getColumn();
		}
		return 0;
	}

	public int getSheetRow(int sheet) {
		if (0 <= sheet && sheet < s_nSheet) {
			return s_awSheets[sheet].getRow();
		}
		return 0;
	}

	public String getSheetCell(int sheet, int col, int row) {
		if (0 <= sheet && sheet < s_nSheet) {
			return s_awSheets[sheet].getCell(col, row);
		}
		return "";
	}

	public int getSheetType() {
		return s_nSheetType;
	}

	public boolean IsOdsFile() {
		if (s_nSheetType == SHEET_TYPE_ODS) {
			return true;
		}

		return false;
	}

	// //////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////
	// Static methods
	// Headers
	public static final int SHEET_TYPE_INVALID = -1;
	public static final int SHEET_TYPE_ODS = 0;
	public static final int SHEET_TYPE_ODS_1 = 1;
	public static final int SHEET_TYPE_XLS = 2;
	public static final int SHEET_TYPE_XML = 3;

	public static final int SHEET_TYPE_NUMBER = 4;

	public static final int MAX_HEADER_SIZE = 8;

	public static final byte[][] s_abSheetTypeHeader = {
			{ (byte) 0x50, (byte) 0x4B, (byte) 0x03, (byte) 0x04, (byte) 0x14, },
			{ (byte) 0x50, (byte) 0x4B, (byte) 0x03, (byte) 0x04, (byte) 0x0A, },
			{ (byte) 0xD0, (byte) 0xCF, (byte) 0x11, (byte) 0xE0, (byte) 0xA1,
					(byte) 0xB1, (byte) 0x1A, (byte) 0xE1, },
			{ '<', '?', 'x', 'm', 'l', }, };

	public static int getSheetType(byte[] data) {
		for (int i = 0; i < SHEET_TYPE_NUMBER; i++) {
			if (data[0] == s_abSheetTypeHeader[i][0]) {
				for (int j = 1; j < s_abSheetTypeHeader[i].length
						&& j < data.length; j++) {
					if (data[j] != s_abSheetTypeHeader[i][j]) {
						break;
					}
					if (j == s_abSheetTypeHeader[i].length - 1) {
						return i;
					}
				}
			}
		}
		return SHEET_TYPE_INVALID;
	}

	public static WorkSheets getWorkSheetsByString(byte[] fileData) {
		try {
			// Read the header
			int type = getSheetType(fileData);
			WorkSheets worksheet = null;
			if (type == SHEET_TYPE_XLS) {
				// worksheet = XLSReader.parse(strFilename);
				// worksheet.s_nSheetType = SHEET_TYPE_XLS;
			} else if ((type == SHEET_TYPE_ODS) || (type == SHEET_TYPE_ODS_1)) {
				worksheet = ODSReader.parseByString(fileData);
				worksheet.s_nSheetType = SHEET_TYPE_ODS;
			} else if (type == SHEET_TYPE_XML) {
				// worksheet = XMLReader.parse(strFilename);
				// worksheet.s_nSheetType = SHEET_TYPE_XML;
			} else {
				Log.d("ods reader", "This format hasn't supported yet");
			}
			return worksheet;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static WorkSheets getWorkSheets(String strFilename) {
		try {
			System.out.println("---strFilename " + strFilename);
			// Read the header
			FileInputStream dis = new FileInputStream(strFilename);
			byte[] data = new byte[MAX_HEADER_SIZE];
			dis.read(data);
			int type = getSheetType(data);
			dis.close();
			dis = null;
			WorkSheets worksheet = null;
			if (type == SHEET_TYPE_XLS) {
				// worksheet = XLSReader.parse(strFilename);
				// worksheet.s_nSheetType = SHEET_TYPE_XLS;
			} else if ((type == SHEET_TYPE_ODS) || (type == SHEET_TYPE_ODS_1)) {
				worksheet = ODSReader.parse(strFilename);
				worksheet.s_nSheetType = SHEET_TYPE_ODS;
			} else if (type == SHEET_TYPE_XML) {
				// worksheet = XMLReader.parse(strFilename);
				// worksheet.s_nSheetType = SHEET_TYPE_XML;
			} else {
				Log.d("ods reader", "This format hasn't supported yet");
			}
			System.out.println("---strFilename ok" + strFilename);
			return worksheet;
		} catch (Exception ex) {
			System.out.println("---strFilename ex " + ex.getMessage());
			ex.printStackTrace();
		}
		return null;
	}

	public static String getCellName(int col, int row) {
		int col1 = col + 1;
		String str = "";
		while (col1 > 0) {
			char ch = (char) ('A' + (((col1 - 1) % 26)));
			str = ("" + ch) + str;
			col1 = (col1 - 1) / 26;
		}
		str += (row + 1);
		return str;
	}

	public static int getColumnFromCellName(String str) {
		int length = str.length();
		for (int i = 0; i < length; i++) {
			char ch = str.charAt(i);
			if ('0' <= ch && ch <= '9') {
				int col = 1;
				for (int j = i - 1; j >= 0; j--) {
					int n = (str.charAt(j) - 'A' + 1);

					if (j == i - 1) {
						col *= n;
					} else {
						col += (n * 26);
					}
				}
				return col - 1;
			}
		}
		return -1;
	}

	public static int getRowFromCellName(String str) {
		try {
			int length = str.length();
			for (int i = 0; i < length; i++) {
				char ch = str.charAt(i);
				if ('0' <= ch && ch <= '9') {
					return new Integer(str.substring(i)).intValue() - 1;
				}
			}
		} catch (Exception ex) {
		}
		return -1;
	}
}
