package com.odsReader.reader;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

//import spreadsheet.*;

public class ODSReader {
    public static final int MAX_COLUMNS = 512;
    public static final int MAX_ROWS = 1024;

    public static WorkSheets parseByString(byte[] fileData) throws Exception {

//		InputStream stream = new ByteArrayInputStream(fileData.getBytes("UTF-8"));
        InputStream stream = new ByteArrayInputStream(fileData);


        ZipInputStream zip = new ZipInputStream(stream);
        ZipEntry ze = null;
        do {
            ze = zip.getNextEntry();
        } while (!ze.getName().equals("content.xml"));

        // Use an instance of ourselves as the SAX event handler
        ODSHandleParser handler = new ODSHandleParser();
        // handler.reset();

        // Use the default (non-validating) parser
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(zip, handler);
        } catch (org.xml.sax.SAXParseException pe) {
            // Utils.ERR("ODSReader::SAXParseException:: " + pe);
            // Utils.LOG("col,line:"+pe.getColumnNumber()+","+pe.getLineNumber()
            // +" "+pe.getPublicId()+" "+pe.getSystemId());
        }

        int size = ODSHandleParser.s_vectorWorkSheets.size();
        WorkSheet[] sheets = new WorkSheet[size];
        for (int i = 0; i < size; i++) {
            sheets[i] = ODSHandleParser.s_vectorWorkSheets.elementAt(i);
        }
        return new WorkSheets(sheets);
    }

    public static WorkSheets parse(String filename) throws Exception {
        FileInputStream dis = new FileInputStream(filename);
        if (dis == null) {
            // Utils.LOG("ERROR: could not find " + filename + " file");
        }
        ZipInputStream zip = new ZipInputStream(dis);
        ZipEntry ze = null;
        do {
            ze = zip.getNextEntry();
        } while (!ze.getName().equals("content.xml"));

        // Use an instance of ourselves as the SAX event handler
        ODSHandleParser handler = new ODSHandleParser();
        // handler.reset();

        // Use the default (non-validating) parser
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(zip, handler);
        } catch (org.xml.sax.SAXParseException pe) {
            // Utils.ERR("ODSReader::SAXParseException:: " + pe);
            // Utils.LOG("col,line:"+pe.getColumnNumber()+","+pe.getLineNumber()
            // +" "+pe.getPublicId()+" "+pe.getSystemId());
        }

        int size = ODSHandleParser.s_vectorWorkSheets.size();
        WorkSheet[] sheets = new WorkSheet[size];
        for (int i = 0; i < size; i++) {
            sheets[i] = ODSHandleParser.s_vectorWorkSheets.elementAt(i);
        }
        return new WorkSheets(sheets);
    }
}

class ODSHandleParser extends DefaultHandler {
    private static WorkSheet s_curWorkSheet;
    public static Vector<WorkSheet> s_vectorWorkSheets;
    private static Vector<Vector<String>> s_vectorRows;
    private static Vector<String> s_vectorValues;
    private static int s_nMaxColumn;
    private static int s_nColumnCounter;
    private static int s_nColumnRepeat;
    private static boolean s_isStartedRow;
    private static boolean s_isStartedCell;
    private static boolean s_isStartedValue;
    private static boolean s_isStartedComment;
    private static String s_strValue = null;

    // Debug only
    private static boolean s_isVerbose = !true;
    private static int s_nTabNumber;
    private static boolean s_bIsGetData;
    private static boolean s_bStartedQname;
    private static String s_strLastQname;

    // End debug

    public ODSHandleParser() {
        s_vectorWorkSheets = new Vector<>();
        s_nMaxColumn = 0;
    }

    // ===========================================================
    // SAX DocumentHandler methods
    // ===========================================================
    @Override
    public void characters(char[] buf, int offset, int len) {
        if (s_strValue != null && s_isStartedValue) {
            String strValue = new String(buf, offset, len);
            s_strValue += strValue;
        }
        if (s_isVerbose) {
            System.out.print(new String(buf, offset, len));
            s_bIsGetData = true;
        }
    }

    @Override
    public void startElement(String namespaceURI, String lName, String qname,
                             Attributes attrs) {
        if (s_isVerbose) {
//            if (s_bStartedQname) {
//            }
            for (int i = 0; i < s_nTabNumber; i++) {
                System.out.print("\t");
            }
            System.out.print("<" + qname);
            int length = attrs.getLength();
            for (int i = 0; i < length; i++) {
                String strQName = attrs.getQName(i);
                System.out.print(" " + strQName + "=\""
                        + attrs.getValue(strQName) + "\"");
            }
            System.out.print(">");
            s_nTabNumber++;
            s_bStartedQname = true;
        }

        if (qname.equalsIgnoreCase("table:table-row")) {
            s_vectorValues = new Vector<String>();
            String param = attrs.getValue("table:number-rows-repeated");
            if (param != null) {
                int nJump = Integer.parseInt(param);
                Vector<String> vectorEmpty = new Vector<String>();
                if (nJump > ODSReader.MAX_ROWS) {
                    nJump = ODSReader.MAX_ROWS;
                }
                for (int i = 0; i < nJump - 1; i++) {
                    s_vectorRows.add(vectorEmpty);
                }
            }
            s_isStartedRow = true;
            s_nColumnCounter = 0;
        } else if (qname.equalsIgnoreCase("table:table-cell")) {
            if (s_isStartedRow) {
                String param = attrs.getValue("table:number-columns-repeated");
                s_nColumnRepeat = 1;
                if (param != null) {
                    s_nColumnRepeat = (new Integer(param)).intValue();
                }
                s_nColumnCounter += s_nColumnRepeat;
                s_isStartedCell = true;
            }
        } else if (qname.equalsIgnoreCase("text:p")) {
            if (!s_isStartedComment) {
                if (s_isStartedCell) {
                    if (s_strValue != null) {
                        s_strValue += "\n";
                    } else {
                        s_strValue = new String("");
                    }
                    s_isStartedValue = true;
                }
            }
        } else if (qname.equalsIgnoreCase("office:annotation")) {
            s_isStartedComment = true;
        } else if (qname.equalsIgnoreCase("table:table")) {
            s_vectorRows = new Vector<>();
            s_curWorkSheet = new WorkSheet();
            s_curWorkSheet.setName(attrs.getValue("table:name"));
        }
    }

    @Override
    public void endElement(String namespaceURI, String sName, String qname)
              {
        // Debug start
        if (s_isVerbose) {
            s_nTabNumber--;
            if (!s_bIsGetData) {
                s_bIsGetData = false;
            } else {
                for (int i = 0; i < s_nTabNumber; i++) {
                    System.out.print("\t");
                }
            }
            s_bStartedQname = false;
        }
        // Debug end

        if (qname.equalsIgnoreCase("table:table-row")) {
            s_vectorRows.add(s_vectorValues);
            s_vectorValues = null;
            s_isStartedRow = false;
            if (s_nMaxColumn < s_nColumnCounter) {
                s_nMaxColumn = s_nColumnCounter;
            }
            s_nColumnCounter = 0;
        } else if (qname.equalsIgnoreCase("table:table-cell")) {
            s_isStartedCell = false;
            String str = s_strValue;
            if (str == null) {
                str = "";
            }
            if (s_nColumnRepeat > ODSReader.MAX_COLUMNS) {
                s_nColumnRepeat = ODSReader.MAX_COLUMNS;
            }
            for (int i = 0; i < s_nColumnRepeat; i++) {
                s_vectorValues.add(str);
            }
            s_strValue = null;
        } else if (qname.equalsIgnoreCase("text:p")) {
            if (!s_isStartedComment) {
                s_isStartedValue = false;
            }
        } else if (qname.equalsIgnoreCase("office:annotation")) {
            s_isStartedComment = false;
        } else if (qname.equalsIgnoreCase("table:table")) {
            int nRows = s_vectorRows.size();
            int nCols = s_nMaxColumn;
            s_curWorkSheet.setSize(nCols, nRows);
            for (int row = 0; row < nRows; row++) {
                Vector<String> values = s_vectorRows.elementAt(row);
                int size = values.size();
                for (int col = 0; col < nCols; col++) {
                    String strValue = "";
                    if (col < size) {
                        strValue = values.elementAt(col);
                    }
                    s_curWorkSheet.setCell(strValue, col, row);
                }
            }
            s_vectorRows = null;
            s_vectorWorkSheets.add(s_curWorkSheet);
            s_curWorkSheet = null;
            s_nMaxColumn = 0;
        }
    }
}
