
package com.odsReader.reader;

public class WorkSheet
{
    public String[][]           m_asCellValues;
    private String              m_strSheetName = "Sheet";
    private int                 m_nRows;
    private int                 m_nCols;
    private int                 m_nCurRow;
    private int                 m_nCurCol;
    
    final int                   MAX_CELL_SIZE = 1024;
	final int                   MAX_ROW_SIZE = 4096;

    public WorkSheet()
    {
    }

    public WorkSheet(int nCols, int nRows)
    {
        setSize(nCols, nRows);
    }

    public void init()
    {
        m_asCellValues = new String[MAX_CELL_SIZE][MAX_ROW_SIZE];
    }

    public void setSize(int nCols, int nRows)
    {
        if (nRows > 0 && nCols > 0)
        {
            m_asCellValues = new String[nCols][nRows];
            m_nRows = nRows;
            m_nCols = nCols;
        }
    }

    public void setName(String name)
    {
        m_strSheetName = name;
    }
    
    public String getName()
    {
        return m_strSheetName;
    }
    
    public void newRow()
    {
        m_nCurRow++;
        m_nCurCol = 0;
        m_nRows = m_nCurRow + 1;
    }
    
    public void packTable(boolean isODSFormat)
    {
        int offsetR = isODSFormat ? 1 : 0;
        String[][] table2 = new String[m_nCols][m_nRows];
		for (int i=0; i<m_nCols; i++)
			for (int j=0; j<m_nRows; j++)
				table2[i][j] = m_asCellValues[i][j + offsetR];
		m_asCellValues = table2;
		System.gc();
    }

    public void setTable(String value, int nbRepeat, int spanAccrossCell, int spanAcrossRow)
	{
		if (m_asCellValues == null)
			return;

		for (int i=0; i<spanAccrossCell*nbRepeat; i++)
		{
			while (m_asCellValues[m_nCurCol][m_nCurRow] != null)
				m_nCurCol++;
			for (int j=0; j<spanAcrossRow; j++)
			{
				m_asCellValues[m_nCurCol][m_nCurRow+j] = value;
			}
			m_nCurCol++;
		}

		if (m_nCurCol > m_nCols)
			m_nCols = m_nCurCol;
	}

    public void setCell(String value, int nCol, int nRow)
    {
        if (0 <= nRow && nRow < m_nRows && 0 <= nCol && nCol < m_nCols)
        {
            m_asCellValues[nCol][nRow] = value;
        }
    }

    public String getCell(int nCol, int nRow)
    {
        if (0 <= nRow && nRow < m_nRows && 0 <= nCol && nCol < m_nCols && m_asCellValues[nCol][nRow] != null)
        {
            return m_asCellValues[nCol][nRow];
        }
        return "";
    }
    
    public int getColumn()
    {
        return m_nCols;
    }

    public int getRow()
    {
        return m_nRows;
    }
}
